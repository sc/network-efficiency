from math import sin, cos, radians, asin, sqrt
import networkx as nx
from networkx.algorithms.shortest_paths.weighted import dijkstra_path_length
from networkx.algorithms.shortest_paths.weighted import all_pairs_dijkstra_path_length, dijkstra_path_length
#import matplotlib.pyplot as plt
import numpy as np

def point_geodist(p1, p2):
    lon0 = p1['longitude']
    lat0 = p1['latitude']
    lon1 = p2['longitude']
    lat1 = p2['latitude']
    lon0, lat0, lon1, lat1 = map(radians, [lon0, lat0, lon1, lat1])
    # haversine formula
    dlon = lon1 - lon0
    dlat = lat1 - lat0
    a = sin(0.5*dlat) ** 2 + cos(lat0) * cos(lat1) * sin(0.5*dlon) ** 2
    c = 2 * asin(sqrt(a))
    # 6367 is the radius of the Earth in km
    km = 6367 * c
    return km

def point_eucldist(p1, p2):
    x1 = p1['pos_x']
    x2 = p2['pos_x']
    y1 = p1['pos_y']
    y2 = p2['pos_y']
    d = sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
    #d = sqrt(((x1 - x2)*(x1 - x2)) + ((y1 - y2)*(y1 - y2)))
    return d

def dist_dict(G, eucl=False):

    nod = G.nodes()
    N = len(nod)
    distances = {x: {y:0 for y in nod} for x in nod}
    if eucl == False:
        for i in xrange(N):
            node1 = nod[i]
            p1 = G.node[node1]
            for j in xrange(i+1, N):
                node2 = nod[j]
                p2 = G.node[node2]
                d = point_geodist(p1, p2)
                distances[node1][node2] = d
                distances[node2][node1] = d
    else:
        for i in xrange(N):
            node1 = nod[i]
            p1 = G.node[node1]
            for j in xrange(i+1, N):
                node2 = nod[j]
                p2 = G.node[node2]
                d = point_eucldist(p1, p2)
                distances[node1][node2] = d
                distances[node2][node1] = d
    return distances

def spl_matrix(G, L=-1):
    # initialize dictionary: all SPL -1 (unreachable) except elements i,i = 0
    nod = G.nodes()
    N = len(nod)
    SP = {x: {y: L if x != y else 0 for y in nod} for x in nod}
    nSP = {x: {y: [] for y in nod} for x in nod}
    for i in xrange(N):
        n1 = nod[i]
        for j in xrange(i+1, N):
            n2 = nod[j]
            try:
                aux1, aux2 = nx.single_source_dijkstra(G, n1, n2, weight='weight')
                path_length = aux1[n2]
                path = aux2[n2]
                SP[n1][n2] = path_length
                SP[n2][n1] = path_length
                nSP[n1][n2] = path
                nSP[n2][n1] = path
            except nx.NetworkXNoPath:
                pass
    return SP, nSP

def spl_matrix_full(G, L=-1):
    # initialize dictionary: all SPL -1 (unreachable) except elements i,i = 0
    nod = G.nodes()
    N = len(nod)
    SP = {x: {y: L if x != y else 0 for y in nod} for x in nod}
    nSP = {x: {y: [] for y in nod} for x in nod}
    for n in nod:
        aux1, aux2 = nx.algorithms.shortest_paths.single_source_dijkstra(G, n, target=None, weight='weight')
        SP[n] = aux1
        nSP[n] = aux2
    for n1 in nod:
        for n2 in nod:
            if n2 not in SP[n1].keys():
                SP[n1][n2] = 0
    return SP, nSP

# updates shortest path length matrix (dictionary) when we have added a new link n1-n2
# G is the new graph, spl is the old matrix

def spl_update(G,n1,n2,spl, L=-1): # NOT WORKING
    nod = [x for x in G.nodes() if G.degree(x) >= 1]
    N = len(nod)
    d12 = G[n1][n2]['weight']
    nn1 = {x: L for x in nod}
    nn2 = {x: L for x in nod}
    aux_list = []

    for n in nod:
        try:
            # path length between n (generic) and n1
            nn1[n] = dijkstra_path_length(G, n1, n, weight='weight')
            # path length between n (generic) and n2
            nn2[n] = dijkstra_path_length(G, n2, n, weight='weight')
            aux_list += [n]
        except nx.NetworkXNoPath:
            pass
    spl1 = {}
    for k in spl.keys():
        spl1[k] = {}

    for n in aux_list:
        if n not in spl1.keys():
            spl1[n]={}

    for i in range(len(aux_list)):
        n = aux_list[i]
        for j in range(i+1, len(aux_list)):
            m = aux_list[j]
            try:
                aux = min([x for x in [spl[n][m], nn1[n] + d12 + nn2[m], nn1[m] + d12 + nn2[n]] if x > 0])
            except KeyError:
                aux = min([x for x in [nn1[n] + d12 + nn2[m], nn1[m] + d12 + nn2[n]] if x > 0])
            spl1[n][m] = aux
            spl1[m][n] = aux
            # if spl[n][m]!=spl1[n][m]: print n,m,spl[n][m],spl1[n][m]
    return spl1

def dist_matrix(G, eucl=False):
    distances = []
    nod = G.nodes()
    N = len(nod)
    if eucl == False:
        for i in xrange(N):
            distances += [[]]
            p1 = G.node[nod[i]]
            for j in xrange(N):
                p2 = G.node[nod[j]]
                d = point_geodist(p1, p2)
                distances[i] += [d]
    else:
        for i in xrange(N):
            distances += [[]]
            p1 = G.node[nod[i]]
            for j in xrange(N):
                p2 = G.node[nod[j]]
                d = point_eucldist(p1, p2)
                distances[i] += [d]

    return distances


def global_efficiency_new(G, D, SP, eucl=False):
    """
    take import networkx as nx outside
    :param G: networkx Graph object
    :param D: distance dict
    :param SP: result of nx.algorithms.shortest_paths.weighted.all_pairs_dijkstra_path_length
    :param eucl: True if 'pos_x' and 'pos_y' in G.nodes attributes. False if 'longitude' and 'latitude'
    :return: global efficiency of G (float)
    """
    # import networkx as nx

    N = len(G.nodes())
    nod = G.nodes()
    # Global Efficiency calculation
    total_global=0
    for n1 in nod:
        aux_n2 = [x for x in SP[n1].keys() if nod.index(n1) < nod.index(x)]
        for n2 in aux_n2:
            total_global += D[n1][n2]/SP[n1][n2]
    global_eff = total_global*2./float(N*(N-1))
    return global_eff

# def global_efficiency_update(G, D, spl):
#
#     nod = G.nodes()
#     N=len(nod)
#     # Global Efficiency calculation
#     total_global=0
#     for i in xrange(N):
#         n1 = nod[i]
#         for j in xrange(i+1,N):
#             n2 = nod[j]
#
#             path = float(spl[n1][n2])
#             if path > 0:
#                 total_global += D[n1][n2]/path
#
#     global_eff = total_global*2./float(N*(N-1))
#     return global_eff




def local_efficiency(G, D):
    """
    Many calls to this function. Better to import outside:
    from networkx.algorithms.shortest_paths.weighted import dijsktra_path_length
    import networkx as nx
    :param G: networkx Graph object
    :param D: distance dict
    :param eucl: True if 'pos_x' and 'pos_y' in G.nodes attributes. False if 'longitude' and 'latitude'
    :return: value of the local efficiency
    """

    nod = G.nodes()
    N = len(nod)
    total_local = 0
    aux_nod = [x for x in nod if G.degree(x) > 1]  # If <= 1 does not contribute -> skip step
    for n in aux_nod:
        neigh = G[n]
        neighbors = neigh.keys()
        G.remove_node(n)
        subtotal = 0
        deg = len(neighbors)
        for j in xrange(deg):
            node1 = neighbors[j]
            for k in xrange(j+1, deg):
                node2 = neighbors[k]
                try:
                    path = dijkstra_path_length(G, node1, node2, weight='weight')
                    d = D[node1][node2]
                    subtotal += d/path
                except nx.NetworkXNoPath:
                    pass
        total_local += subtotal*2/(deg*(deg-1))  # divided by the number of neighbor pairs
        G.add_node(n)
        ###ugly way to put again the removed node with correct weight on the edges
        for i in neigh :
            try:
                w=neigh[i]['weight']
            except:
                w=None
            G.add_edges_from([(n,i)],weight=w)


    local_eff = total_local/float(N)
    return local_eff


def empCDF(v):
    w = np.sort(v)
    p = 1. * np.arange(len(w)) / (len(w) - 1)
    return w, p


def mse(v1, v2, x_range):

    p1 = []
    p2 = []
    diff = []
    for x in x_range:

        p1 += [len([k for k in v1 if k <= x]) / float(len(v1))]
        if len(v2) == 0:
            p2 += [0]
        else:
            p2 += [len([k for k in v2 if k <= x]) / float(len(v2))]

    for i in range(len(p1)):
        diff += [(p1[i] - p2[i]) ** 2]
    return np.mean(diff)



def results2graph(G0, D, a):
    """
    Builds a network G with the same layout as G0 from the results array a
    :param G0: networkx Graph object, reference node layout
    :param D: distance dict
    :param a: array of results
    :return:
    """
    G = G0.copy()
    for e in a[1:]:
        n1 = e[-1]
        n2 = e[-2]
        d = D[n1][n2]
        G.add_edge(n1, n2, weight=d)
    return G

def kv_dict(a, N):
    # a must be results of one single run
    kv_list = []
    K = {x: 0 for x in range(N)}
    V = {x: 0 for x in range(N)}
    for e in a[1:]:
        n1 = e[-1]
        n2 = e[-2]
        K[n1] += 1
        K[n2] += 1
        if V[n1] < e[0]:
            V[n1] = e[0]
        if V[n2] < e[0]:
            V[n2] = e[0]
        kv_list += [(K[n1], V[n1], e[1]), (K[n2], V[n2], e[1])]

    k_range = sorted(list(set([x[0] for x in kv_list])))
    kv_dict = {k:[] for k in k_range}
    for k in k_range:
        kv_dict[k] += [x[1] for x in kv_list if x[0] == k]

    return kv_dict



#reutrn a dictionnary with the efficiencies of the networ `G` when adding the edge `edge`
def get_new_efficiencies(G,D,edge,f_i,eucl=True):
    n1,n2=edge
    d = D[n1][n2]
    G.add_edge(n1, n2, weight=d)
    Eff = dict()
    S = all_pairs_dijkstra_path_length(G)
    Eff["eg_aux"] = global_efficiency_new(G, D, S , eucl=True)
    Eff["el_aux"] = local_efficiency(G, D)
    Eff["edge"]=edge
    Eff["E_aux"] = (f_i - sqrt(0.5 * (1 - Eff["eg_aux"]) ** 2 + 0.5 * (1 - Eff["el_aux"]) ** 2)) / d
    Eff["d"]=d
    G.remove_edge(n1, n2)
    return(Eff)

    
