from random_layout import random_layout
from delaunay import eucl_min_distances_tree, DelaunayGraph
from math import sqrt, ceil
import os
import logging
import pickle
import networkx as nx
from networkx.algorithms.shortest_paths.weighted import all_pairs_dijkstra_path_length, dijkstra_path_length
from random import sample
from temp_functions import dist_dict, global_efficiency_new, local_efficiency, spl_matrix_full,results2graph,get_new_efficiencies
import sys

if __name__ == '__main__':

    if len(sys.argv) < 5:
        print("usage:\t python run.py N g kDel threshold [kDel_prev]")
        print("\t\t N : 100,200,50")
        print("\t\t g : [0;100]")
        print("\t\t kDel : [0,1]")
        print("\t\t threshold : TH or noTH")
        print("\t\t kDel_prev : the file that contains data")
        exit(0)

    # From terminal
    N = int(sys.argv[1])
    g = int(sys.argv[2])
    kDel = float(sys.argv[3])
    threshold = sys.argv[4]
    kDel_prev = None


    if len(sys.argv) == 6:
        kDel_prev = str(sys.argv[5])  # assuming tha kDel_prev is the filename

    # From IDE
    # N = 50
    # g = 0
    # kDel = 0.05
    # threshold = 'noTH'
    # kDel_prev = None


    L = -1
    A = 2.34
    B = 4.9
    alpha = 0.5

    jobid="run_"+str(N)+"_"+str(os.getpid())
    logging.basicConfig(format="%(asctime)s;%(levelname)s;%(message)s",filename=str(jobid)+".log",level=logging.INFO)

    # Folders
    layout_folder = os.path.join('layouts/','N' + str(N))
    prev_folder = ''
    output_folder = 'results/'

    # Load layout
    with open(os.path.join(layout_folder,str(g) + '.txt'), 'r') as f:
        G0 = pickle.load(f)
    nod = G0.nodes()
    D = dist_dict(G0, eucl=True)
    sN = {x: [] for x in nod}
    for n in nod:
        sN[n] = sorted([(x, D[n][x]) for x in nod], key=lambda tup: tup[1])
        sN[n] = [x[0] for x in sN[n]]

    V = {x: {y: 0 for y in nod} for x in nod}
    for n1 in nod:
        for n2 in nod:
            v1 = sN[n1].index(n2)
            v2 = sN[n2].index(n1)
            v_min = min(v1, v2)
            V[n1][n2] = v_min

    v_th = [ceil(x * A + B) for x in range(N)]

    if not kDel_prev:
        G = G0.copy()  # initialize network model
        results = [(0, 0, 0, 0, 0, -1, -1)]  # initialize results list
        min_dist = 1e9  # locate shortest distance to be the first link
        for i in xrange(N):
            n1 = nod[i]
            for j in xrange(i + 1, N):
                n2 = nod[j]
                dist = D[n1][n2]
                if dist < min_dist:
                    min_dist = dist
                    min_dist_edge = (n1, n2)

        G.add_edge(min_dist_edge[0], min_dist_edge[1], {'weight': min_dist})
        V[min_dist_edge[0]][min_dist_edge[1]] = N + 1
        V[min_dist_edge[1]][min_dist_edge[0]] = N + 1
        eg = 2 / float(N * (N - 1))  # eg if just one edge
        el = 0.
        total_weight = min_dist
        E = sqrt(alpha * (1 - eg) ** 2 + (1 - alpha) * (1 - el) ** 2)
        results += [(1, min_dist, eg, el, E, min_dist_edge[0], min_dist_edge[1])]
        k = 2
    else:
        # load results
        with open(kDel_prev, 'r') as f:  # build graph G with results
            results = pickle.load(f)

        G = results2graph(G0, D, results)  # load network model
        total_weight = results[-1][1]
        eg = results[-1][2]
        el = results[-1][3]
        E = sqrt(alpha * (1 - eg) ** 2 + (1 - alpha) * (1 - el) ** 2)
        k = len(results)

        for e in G.edges():
            V[e[0]][e[1]] = N + 1
            V[e[1]][e[0]] = N + 1

    #S = all_pairs_dijkstra_path_length(G)

    # Set weight limit
    GDel = DelaunayGraph(G0, eucl=True)  # Set weight limit to DT total weight
    weight_limit = sum([GDel[e[0]][e[1]]['weight'] for e in GDel.edges()]) * kDel

    logging.info("starting main loop")
    while total_weight < weight_limit:
        bestEff=dict()
        bestEff["E_aux"] = -1e9
        f_i = sqrt(0.5 * (1 - results[k - 1][2]) ** 2 + 0.5 * (1 - results[k - 1][3]) ** 2)
        for n1 in nod:
            pos_n1 = nod.index(n1)
            deg_n1 = G.degree(n1)
            neigh = G.neighbors(n1)
            if threshold == 'TH':
                aux_n2 = [x for x in nod[pos_n1 + 1:] if V[n1][x] <= max(v_th[deg_n1], v_th[G.degree(x)])]
            else:
                aux_n2 = [x for x in nod[pos_n1 + 1:] if x not in neigh]
            for n2 in aux_n2:
                #altversion: listedge.append((n1,n2) )
                Eff = get_new_efficiencies(G,D,(n1,n2),f_i,eucl=True)
                if Eff["E_aux"] > bestEff["E_aux"]:
                    bestEff = Eff

        #altversion: all_eff=map(node(listedge),get_new_efficiencies)
        #altversion: Eff=select(max,all_eff)
        eg_next = bestEff["eg_aux"]
        el_next = bestEff["el_aux"]
        E_next = bestEff["E_aux"]
        #S = bestEff["S"]
        edge_next = bestEff["edge"]
        d_next = bestEff["d"]
        G.add_edge(edge_next[0], edge_next[1], weight=d_next)
        total_weight += d_next

        logging.info("cur kdel:"+str(round(total_weight / weight_limit, 3)))
        print(str(round(total_weight / weight_limit, 3)))

        results += [(V[edge_next[0]][edge_next[1]], total_weight, eg_next,
                     el_next, E_next, edge_next[0], edge_next[1])]
        V[edge_next[0]][edge_next[1]] = N + 1
        V[edge_next[1]][edge_next[0]] = N + 1
        k += 1

    logging.debug("writing current state")
    with open(output_folder+'N'+str(N)+'_'+str(g)+'_'+str(threshold)+'_'+str(kDel)+'.txt', 'w') as f:
        pickle.dump(results, f)


