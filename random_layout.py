def random_layout(N,L):
    
    import networkx as nx
    from random import random
    
    G = nx.Graph() # empty graph
    
    for i in range(N):
        x=random()*L
        y=random()*L
        G.add_node(i,{'pos_x':x,'pos_y':y})
    
    return G

def add_random_layout(G,N,L):
    
    from random import random    
    Aux = G.copy()
    for i in range(N):
        x=random()*L
        y=random()*L
        Aux.add_node(i,{'pos_x':x,'pos_y':y})
    
    return Aux

def quadrants_layout(N,L):
    from random import random
    import networkx as nx
    
    G = nx.Graph()
    for i in range(N):
        x=random()*L
        if x < 0.5*L:
            y = 0.5+0.5*random()
        else:
            y = 0.5*random()
        G.add_node(i,{'pos_x':x,'pos_y':y})
    return G

