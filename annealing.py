import os
import pickle
import networkx as nx
from networkx.algorithms.shortest_paths.weighted import all_pairs_dijkstra_path_length, dijkstra_path_length
import random
from temp_functions import dist_dict, global_efficiency_new, local_efficiency, spl_matrix_full
import sys

if __name__ == '__main__':

    if len(sys.argv) < 4:
        print("usage:\t python annealing.py N g")
        print("\t\t N : 100,200,50")
        print("\t\t g : [0;100]")
        print("\t\t steps: integer")
        exit(0)

    # From terminal
    N = int(sys.argv[1])
    g = int(sys.argv[2])
    steps = int(sys.argv[3])

    alpha = 0.5

    with open(os.path.join('layouts/N'+str(N)+'/'+str(g))+'.txt', 'r') as f:
        G = pickle.load(f)
    D = dist_dict(G, eucl=True)
    for i in range(len(G.nodes())):
        n1 = G.nodes()[i]
        for j in range(i+1, len(G.nodes())):
            n2 = G.nodes()[j]
            G.add_edge(n1, n2, {'weight':D[n1][n2]})
    Lcomp = sum([G[x[0]][x[1]]['weight'] for x in G.edges()])  # Length of the complete graph
    S = all_pairs_dijkstra_path_length(G)
    E = 1.0
    L = 1.0

    for s in range(steps):
        print str(s)
        G2 = G.copy()
        if random.random() < 0.5:  # rewiring
            edge = random.choice(G2.edges())
            if random.random() < 0.5:
                n1 = edge[0]
            else:
                n1 = edge[1]
            n2 = random.choice([x for x in G2.nodes() if x != edge[0] and x != edge[1]])
            G2.add_edge(n1, n2, {'weight':D[n1][n2]})
        else:  # add/remove
            pair = random.sample(G2.nodes(), 2)
            if G2.has_edge(pair[0], pair[1]):
                G2.remove_edge(pair[0], pair[1])
            else:
                G2.add_edge(pair[0], pair[1], {'weight':D[pair[0]][pair[1]]})
        S2 = all_pairs_dijkstra_path_length(G2)
        eg2 = global_efficiency_new(G2, D, S2)
        el2 = local_efficiency(G2, D)
        E2 = 1 - sqrt(alpha * (1 - eg) ** 2 + (1 - alpha) * (1 - el) ** 2)
        L2 = sum([G2[x[0]][x[1]]['weight'] for x in G2.edges()]) / Lcomp

        if E2/L2 > E/L:
            G = G2.copy()
            E = E2

        print len(G.edges())
    # G2 Copy of G

    # P(rewiring) = 0.5; P(add/remove) = 0.5
    # If rewiring:
        # choose edge i,j
        # choose k
        # create i,k or j,k at random if not a current edge

    # else:
        # choose pair i,j
        # if i,j exists: remove
        # else: add

    # Calculate obj function
        # keep if better g2 -> g