# Compute network Efficiency
### Requirements: networkx, pickle, delaunay,random_layout


I create a .sh to call run.py with a few parameters

### Summary of run.py
Usage:

`    python ./run.py  N g kDel threshold`


* `N` = system size
* `G0_i` = layout (`i=1..100` empty networks of N nodes)
* `E` = objective function to minimize, definded by Eg and El.

```
for i in [1,100]:
	Load empty network G0 
	Calculate eucl distance matrix for all pairs of nodes
	Set stopping condition: L_max
	Initialize results array
	Add first edge in a copy of G0, G
	E, Eg, El: current efficiencies of the network G
	
	while L < L_max:
		for all non-existent edges:
			Copy previous network G, G2, add edge
			Calculate E_aux(Eg_aux, El_aux) of provisional network G2
			if E_aux better than best E so far:
				E_next = E_aux
				next_edge = Keep info of the edge
		Add next_edge to G
		Update L
		Add next_edge info to results array
		
	write results array when out of the while loop
```

### EXAMPLES:
- `python run.py 50 0 .01 noTH` quick test

### NOTES:
- the for loop over edges should be build first and slice it in an equal number of parts
- calculations of Eg need 1 full dijsktra algorithm
- calculations of El need N full dijsktra algorithms
- Our priority is to obtain results for N=100 and N=200
