# -*- coding: utf-8 -*-
"""
Created on Wed Jul 12 09:51:01 2017

@author: physcomp2
"""

# PROV
# Create Delaunay, Gabriel Graphs from networkx graph 

def DelaunayGraph(G,eucl=False, node_labels=False):
    
    from scipy.spatial import Delaunay
    import numpy as np
    from dist_functions import node_eucldist, node_geodist
    
    if eucl:
        dist = node_eucldist
        pos_x = 'pos_x'
        pos_y = 'pos_y'
    else:
        dist = node_geodist
        pos_x = 'longitude'
        pos_y = 'latitude'
    # Copy set of nodes of the input graph
    GDel = G.copy()
    GDel.remove_edges_from(GDel.edges())
    
    # Generate Delaunay triangulation (object Delaunay)
    points = np.array([[GDel.node[x][pos_x],GDel.node[x][pos_y]] for x in GDel.nodes()])    
    tri = Delaunay(points)
    
    # Fill the graph with the links above
    # TODO does not work if nodes have labels
    if node_labels == False:    
        for t in tri.vertices:
            n1 = t[0]; n2 = t[1]; n3 = t[2]
            if not GDel.has_edge(n1,n2):
                GDel.add_edge(n1,n2,{'weight':dist(GDel,n1,n2)})
            if not GDel.has_edge(n2,n3):
                GDel.add_edge(n2,n3,{'weight':dist(GDel,n2,n3)})
            if not GDel.has_edge(n1,n3):
                GDel.add_edge(n1,n3,{'weight':dist(GDel,n1,n3)})
    else:
        for t in tri.vertices:
            n1 = GDel.nodes()[t[0]]; n2 = GDel.nodes()[t[1]]; n3 = GDel.nodes()[t[2]]
            if not GDel.has_edge(n1,n2):
                GDel.add_edge(n1,n2,{'weight':dist(GDel,n1,n2)})
            if not GDel.has_edge(n2,n3):
                GDel.add_edge(n2,n3,{'weight':dist(GDel,n2,n3)})
            if not GDel.has_edge(n1,n3):
                GDel.add_edge(n1,n3,{'weight':dist(GDel,n1,n3)})
    return GDel



def GabrielGraph(G, GDelaunay=None, eucl=False):
    
    from math import sqrt
    
    # Generate Delaunay graph, from which we subset the Gabriel graph
    if GDelaunay == None:    
        GDel = DelaunayGraph(G,eucl=eucl)
        # or load it if it has been already generated
    else:
        GDel = GDelaunay
    GGab = GDel.copy()
    
    if eucl:
        pos_x = 'pos_x'
        pos_y = 'pos_y'        
    else:
        pos_x = 'longitude'
        pos_y = 'latitude'
    # Iterate over Delaunay edges
    for e in GDel.edges():
        n1 = e[0]; x1 = GGab.node[n1][pos_x]; y1 = GGab.node[n1][pos_y]
        n2 = e[1]; x2 = GGab.node[n2][pos_x]; y2 = GGab.node[n2][pos_y]
        
        # Extract edge's middle point (xc,yc)
        if x1<=x2:
            xc = x1 + (x2-x1)/2. 
        else:
            xc = x2 + (x1-x2)/2.
            
        if y1<=y2:
            yc = y1 + (y2-y1)/2. 
        else:
            yc = y2 + (y1-y2)/2.
            
        d = sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
        
        # if circle centered in (xc,yc) contains at least one other node => remove link and next step
        for n in GGab.nodes():
            if n != e[0] and n != e[1]:        
                xn = GGab.node[n][pos_x]
                yn = GGab.node[n][pos_y]
                
                if (xn-xc)**2+(yn-yc)**2 <= (d/2.)**2:
                    #print n, xn, yn, ' INSIDE'
                    GGab.remove_edge(n1,n2)
                    break
    
    return GGab    
    
def eucl_min_distances_tree(G, G0=None):
    from dist_functions import node_eucldist
    import networkx as nx

    # Empty copy (no links) of the spatial network G or starting from G0
    if G0==None:
        Gm = G.copy()
        Gm.remove_edges_from(G.edges())
    else:
        Gm=G0.copy()

    # Sorted list (ascending) of geographic distances between all pairs of nodes
    D_list = []
    for i in range(len(Gm.nodes())):
        node1 = Gm.nodes()[i]
        for j in range(i + 1, len(Gm.nodes())):
            node2 = Gm.nodes()[j]
            D_list += [(node_eucldist(G, node1, node2), node1, node2)]
            j += 1
    D_list = sorted(D_list)

    # Iterate D_list in order creating a link between node1 and node2 if they
    # re not communicated by any path until a unique connected component is obtained
    i = 0
    t = 0
    while not nx.number_connected_components(Gm) == 1:
        node1 = D_list[i][1]
        node2 = D_list[i][2]
        if not nx.has_path(Gm, node1, node2):
            d = D_list[i][0]
            Gm.add_edge(node1, node2, {'weight': d, 'time': t,'ratio': '0.0'})
            t += 1
        i += 1

    return Gm


# quick drawing
#G = random_layout(50,1)
#GGab = gabrielGraph(G)
#import networkx as nx
#positions = [(GGab.node[x]['pos_x'],GGab.node[x]['pos_y']) for x in GGab.nodes()]
#nx.draw(GGab, pos = positions)
