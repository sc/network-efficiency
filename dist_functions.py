# ==============================================================================
# BASIC FUNCTIONS
# ==============================================================================

# ID 2.1
# baseline function: input = 2 pairs of ordered geographic coordinates
### VERSION MODIFIED BY ALESSIO TO IMPROVE COMPUTATION TIME ###
def coord_geodist(lat0, lon0, lat1, lon1):
    from math import sin, cos, radians, asin, sqrt
    lon0, lat0, lon1, lat1 = map(radians, [lon0, lat0, lon1, lat1])
    # haversine formula
    dlon = lon1 - lon0
    dlat = lat1 - lat0
    a = sin(0.5*dlat) ** 2 + cos(lat0) * cos(lat1) * sin(0.5*dlon) ** 2
    c = 2 * asin(sqrt(a))
    # 6367 is the radius of theEarth in km
    km = 6367 * c
    return km


# ==============================================================================


# ID 2.2
# baseline function: input = 2 dictonaries whose keys are "latitude" and "longitude"
### VERSION MODIFIED BY ALESSIO TO IMPROVE COMPUTATION TIME ###
def point_geodist(p1, p2):
    from math import sin, cos, radians, asin, sqrt
    lon0 = p1['longitude']
    lat0 = p1['latitude']
    lon1 = p2['longitude']
    lat1 = p2['latitude']
    lon0, lat0, lon1, lat1 = map(radians, [lon0, lat0, lon1, lat1])
    # haversine formula
    dlon = lon1 - lon0
    dlat = lat1 - lat0
    a = sin(0.5*dlat) ** 2 + cos(lat0) * cos(lat1) * sin(0.5*dlon) ** 2
    c = 2 * asin(sqrt(a))
    # 6367 is the radius of the Earth in km
    km = 6367 * c
    return km


###########################
# EUCLIDEAN GEOMETRY VARIANT
###########################

# ID 2.3
# baseline function: input = 2 pairs of ordered geographic coordinates
### VERSION MODIFIED BY ALESSIO TO IMPROVE COMPUTATION TIME ###
def coord_eucldist(x1, y1, x2, y2):
    from math import sqrt
    d = sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
    #d = sqrt(((x1 - x2)*(x1 - x2)) + ((y1 - y2)*(y1 - y2)))
    return d


# ==============================================================================

# ID 2.4
# baseline function: input = 2 dictonaries whose keys are "pos_x" and "pos_y"
### VERSION MODIFIED BY ALESSIO TO IMPROVE COMPUTATION TIME ###
def point_eucldist(p1, p2):
    from math import sqrt
    x1 = p1['pos_x']
    x2 = p2['pos_x']
    y1 = p1['pos_y']
    y2 = p2['pos_y']
    d = sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)
    #d = sqrt(((x1 - x2)*(x1 - x2)) + ((y1 - y2)*(y1 - y2)))
    return d


# ==============================================================================
# NODE DISTANCE BASIC FUNCTIONS
# ==============================================================================

# ID 2.5
# distance between two nodes, node1 and node2, of a graph G. Input= nodes' names: node1,2=G.nodes()[i,j]
def node_geodist(G, node1, node2):
    p1 = G.node[node1]
    p2 = G.node[node2]
    km = point_geodist(p1, p2)
    return km


# ==============================================================================

# ID 2.6
# EUCLIDEAN GEOMETRY VARIANT
def node_eucldist(G, node1, node2):
    p1 = G.node[node1]
    p2 = G.node[node2]
    d = point_eucldist(p1, p2)
    return d


# ==============================================================================

# ID 2.7
# takes a graph with geoloc nodes but without 'weight' edge attribute; calc & assign distances as 'weight' edge attribute
def add_weights(G):
    G_aux = G.copy()
    for e in G_aux.edges():
        G_aux[e[0]][e[1]]['weight'] = node_geodist(G_aux, e[0], e[1])

    return G_aux


# ==============================================================================
# DISTANCE MATRIX FUNCTIONS
# ==============================================================================

# ID 2.8
## output: matrix of distances.
## The element [i][j] = distance between node i and node j
## NEW VERSION OPTIMIZED BY ALESSIO ##
def dist_matrix(G, eucl=False):
    distances = []
    nod = G.nodes()
    N = len(nod)
    if eucl == False:
        for i in xrange(N):
            distances += [[]]
            p1 = G.node[nod[i]]
            for j in xrange(N):
                p2 = G.node[nod[j]]
                d = point_geodist(p1, p2)
                distances[i] += [d]
    else:
        for i in xrange(N):
            distances += [[]]
            p1 = G.node[nod[i]]
            for j in xrange(N):
                p2 = G.node[nod[j]]
                d = point_eucldist(p1, p2)
                distances[i] += [d]

    return distances



# ==============================================================================

# ID 2.9
## output: a dictionary of distances.
## The value of distances[node i][node j] = distance between node i and node j
def dist_dict(G, eucl=False):
    distances = {}
    nod = G.nodes()
    N = len(nod)
    if eucl == False:
        for i in xrange(N):
            node1 = nod[i]
            p1 = G.node[node1]
            distances[node1] = {}
            for j in xrange(N):
                node2 = nod[j]
                p2 = G.node[node2]
                d = point_geodist(p1, p2)
                distances[node1][node2] = d
    else:
        for i in xrange(N):
            node1 = nod[i]
            p1 = G.node[node1]
            distances[node1] = {}
            for j in xrange(N):
                node2 = nod[j]
                p2 = G.node[node2]
                d = point_eucldist(p1, p2)
                distances[node1][node2] = d
    return distances


# ==============================================================================

# ID 2.10
##output: a dictionary.
## For each node (key), the list of the rest of nodes sorted by distance and their respective distance
## (a dictionary whose key are node names tand whose values are lists of tuples (dist, node). Diagonal elements included.)
def sort_distances(G, eucl=False):
    D = {}
    for node1 in G.nodes():
        aux = []
        for node2 in G.nodes():
            if node1 == node2:
                d = 0
            else:
                if eucl == False:
                    d = node_geodist(G, node1, node2)
                else:
                    d = node_eucldist(G, node1, node2)

            aux += [(d, node2)]
        aux = sorted(aux)
        D[node1] = aux

    return D


# ==============================================================================

# ID 2.11
## output= a list of lists.
## For each node index, the list of the indexes of its neighbors and their respective distance: a list of tuples (dist, node index).
## Diagonal elements included.
def dist_list(G, eucl=False):
    D = []
    N = len(G.nodes())
    for i in range(N):
        D += [[]]
        p1 = G.node[G.nodes()[i]]
        for j in range(N):
            p2 = G.node[G.nodes()[j]]
            if eucl == False: d = point_geodist(p1, p2)
            if eucl == True: d = point_eucldist(p1, p2)
            D[i] += [(d, j)]
    return D


# ==============================================================================
# DISTANCE STATISTICS FUNCTIONS
# ==============================================================================
# statistics of distance distribution: input = a path to a gdf file; output = a list conteining 8 elements = n (number of pairs), m (mean value), std, dmax, dmin, 1sr, 2nd and 3rd quartile.

# ID 2.12
def distat(filepath, bplt=False, eucl=False):
    from io_functions import gdf_to_nxgraph
    import statistics as stat
    G = gdf_to_nxgraph(filepath)
    aux = dist_matrix(G, eucl)

    D = []
    for i in range(len(aux)):
        for j in range(i + 1, len(aux)):
            D.append(aux[i][j])
    if bplt == True:
        import matplotlib.pyplot as plt
        plt.boxplot(D)

    m = stat.mean(D)
    std = stat.pstdev(D)
    dmax = max(D)
    dmin = min(D)
    D = sorted(D)
    n = len(D)
    q1 = D[int(n / 4.)]
    q2 = D[int(n / 2.)]
    q3 = D[int(3 * n / 4.)]
    v = [n, m, std, dmax, dmin, q1, q2, q3]
    return v

# v=distat("gdf_rdist/roads/EIA1E_road_rdist.gdf")
# ==============================================================================

# ID 2.13
##input: a dictionary.
##output: a list.
## For each node (key), the list of the rest of nodes sorted by distance and their respective distance
## (a dictionary whose key are node names tand whose values are lists of tuples (dist, node). Diagonal elements included.)
def sort_dist_from_dict(placedict, eucl=False):
    D = []
    for p1 in placedict.keys():
        for p2 in placedict.keys():
            if p1 != p2:
                if eucl == False:
                    d = point_geodist(placedict[p1], placedict[p2])
                else:
                    d = point_eucldist(placedict[p1], placedict[p2])
                D += [(d,p1,p2)]
    D = sorted(D)

    return D
